Repository for ROPE, the Construct 3 indie puzzle game developed by ROPE Team.

Play online at https://moofemp.com/rope/

Instructions for desktop: Run compiled\win64\ROPE.exe

ROPE Team is:
Danielle
Holly
Maya

with assistance from:
Kat
Matt
Paul

Licenced under CC-BY-NC-ND 4.0.